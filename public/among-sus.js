const AGENTS = [
    "Blue",
    "Red", 
    "Orange",
    "Yellow",
    "Pink",
    "Cyan",
    "White",
    "Black",
    "Brown",
    "Green"
]

const LOCATIONS = [
    "Medbay",
    "Electrical",
    "O2",
    "Admin",
    "Security",
    "Nav",
    "Reactor",
    "Specimen"
]

const REPLACERS = [
    {"searchString": "{AGENTS}",
     "replaceArray": AGENTS
    },
    {
    "searchString": "{LOCATIONS}",
     "replaceArray": LOCATIONS
    }
].map(function(entry) {
    return function(inputString) {
        let outputString = inputString;
        while (outputString.indexOf(entry.searchString) != -1) {
            let selection = randomChoiceFromList(entry.replaceArray);
            usedFields.push(selection);
            outputString = outputString.replace(entry.searchString, selection);
        }
        return outputString
    }
});

function applyReplacements(inputString) {
    let outputString = inputString;
    for (var replacer of REPLACERS) {
        outputString = replacer(outputString);
    }
    return outputString
}

const NEUTRAL_PREFACES = [
]

const CLEAR_PREFACES = NEUTRAL_PREFACES.concat([
    "I can hard clear {AGENTS}"    
])

const GUILTY_PREFACES = NEUTRAL_PREFACES.concat([
   "I'm pretty sus of {AGENTS}",
   "It's 100% {AGENTS}"    
])

const NEUTRAL_REASONS = [
    ", I was on their ass the whole time",
    ", because they always do the doors as impostor",
    ", because noone calls comms"
]

const GUILTY_REASONS = NEUTRAL_REASONS.concat([
    ", I saw them vent",
    " -- I heard them say wow at the start of the round",
    ", they didn't come to {LOCATIONS} at the start",
    ", I saw them kill {AGENTS}"
])

const CLEAR_REASONS = NEUTRAL_REASONS.concat([
    ", we did {LOCATIONS} together",
    ", we're electrical gang"
])

/* Tuple of (arbitrarily)
    "name": [STRING||[STRING]]
    The order is important in the list, and repetition is allowed
*/
const SENTENCE_STRUCTURES = {
    "accusation":[GUILTY_PREFACES,GUILTY_REASONS,'.'],
    "clear":[CLEAR_PREFACES,CLEAR_REASONS,'.']
}

// This is a list of "used" objects each time a sentence is built
// We will check it for duplication each time to avoid any obvious repetitions
var usedFields = [];

function randomChoiceFromList(list) {
    return list[Math.floor(Math.random()*list.length)]
}

function randomChoiceFromObject(obj) { 
    return obj[randomChoiceFromList(Object.keys(obj))]
}

function generateSentence() {
    usedFields = [];
    let structure = randomChoiceFromObject(SENTENCE_STRUCTURES)
    
    let output = [];
    for (var component of structure) {
        if (typeof(component) == "string") { output.push(component) }
        else if (typeof(component) == "object") { 
            let field = randomChoiceFromList(component);
            usedFields.push(field);
            output.push(applyReplacements(field));
        }
        else { console.log("Error when analysing component: "+component+", for structure: "+structure)}
    }

    // It's possible that we rolled the same thing twice (most notably, agent or location names)
    // So we check output for uniqueness before we squash it down to a string
    // Spooky and unneeded recursion!
    if ([...new Set(usedFields)].length != usedFields.length) {
        return generateSentence()
    }

    return output.join('')
}

function displayNewSentence() {
    let newSentence = generateSentence();

    document.getElementById('sentenceBox').innerHTML = newSentence;
}